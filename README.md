
# Remote file access system

This project is remote file access system project, making use of the [duck networking library](https://gitlab.com/banilaste/duck).
It allows to manage files on a server (by reading, writing...) remotely using the client.

## Installation
### Requirements
- Git for cloning the project *(or download this project zip)*
- Npm for dependencies and building the javascript files
- Node.JS for running the project

During develeppement, npm 6.9 and nodejs 12 were used.

### Building the project
To build the project, you can use the following commands
```bash
git clone https://gitlab.com/banilaste/file-share-node.git
cd file-share-node
npm install
```
This will clone the project into the current directory, download all the dependencies and finally compile the typescript files for the client.

If you want to export the project into a single javascript file, you can in addition use `npm run build`, which will output the client program into the `dist/index.js` file.

## Running
From the command line, you can now start the client using npm or node directly.

### Command line arguments
These command line arguments are used for both the client and the server.

| Argument name | Values | Default | Description | Example |
|---|---|---|---|---|
| logLevel | VERBOSE, DEBUG, WARNING, INFO, ERROR or RESULT | WARN | Sets the logging level to the given threshold. All the logs with levels less important than the threshold are not displayed. | \-\-logLevel=VERBOSE |
|failRate|From 0 to 1|0|Sets the failure rate for the sent datagrams.|-\-failRate=0.33|
|localIp|Ip address|127.0.0.1|Sets the public ip to use for hosted remote objects. **This argument has to be specified if the server and the client are not both on the same machine, otherwise the _watch_ command may not work.**|-\-localIp=192.168.1.38|
|localPort|From 0 to 65535|11111|Sets the port used by the client.|-\-localPort=33333|
|remoteIp|Ip address||Sets the ip of the server.|-\-remoteIp=192.168.1.40|
|remotePort|From 0 to 65535||Sets the server port used by the server.|-\-remotePort=54321|
|cachingTimeout|Any positive integer|10000|Sets the timeout for expiration of the validity of the file cache, in milliseconds.|-\-cachingTimeout=20000|


### Running the client

The startup file for the client is `index.js`, you can either run it with `node index.ts (arguments)` or with `npm start -- (arguments)` (note that `--` is mandatory in order to have npm transmit the arguments to the javascript file).

You can run the client without any argument or specify above arguments to customize some properties, some of them will be asked anyway if you do not provide them.
```bash
# Default settings, you will have to input the server ip and port
npm start

# Server at 192.168.1.40:54321, verbose logging and caching timeout of 15s
npm start -- --remoteIp=192.168.1.40 --remotePort=54321 --logLevel=VERBOSE --cachingTimeout=15000

# Same with node directly
node index.js --remoteIp=192.168.1.40 --remotePort=54321 --logLevel=VERBOSE --cachingTimeout=15000
```

## Command list

Here is the list of commands you can use in the command line interface. Arguments with default values (_a = 2_) are optional (you can just press enter on the command line interface to have the default value).

| Name | Arguments | Description|
|---|---|---|
|**read**| file path, offset = 0, _size = remaining length_ |read a file from given offset if provided|
|**write**| file path, _offset = 0_ | write content in a file from a given offset, create file is it does exists|
|**insert**| file path, _offset = 0_ | insert content in a file from a given offset|
|**ls** | _file path = /_ | list content of a directory|
|**mkdir**| file path | create recursively a directory|
|**watch**| file path, duration | watch file content for a given ms duration|
|**sizeof**| file path | returns the size of a file in bytes|
|**crypt**| file path, password | crypt file using a vigenere like algorithm (really secure !)|
|**decrypt**| file path, password | decrypt file using a vigenere like algorithm|
|**delete**| file path | delete file|
