import { FileManagerStub } from "../../target/generated/remote-stubs"
import CacheEntry, { CacheHolder } from "./cache-entry"
import { Modification, Metadata } from "../../target/generated/remote-objects"
import CachedChunk from "./cached-chunk"
import vigenere from "../../common/vigenere"


/**
 * Extension of the default stub for file manager, adding cache capability
 */
export default class CachedFileManagerStub extends FileManagerStub {
    async read(path: string, offset: bigint, size: number): Promise<Buffer> {
        // Try to read cache
        var result = await this.validReadingOf(
            path, offset, size
        )

        // or read from remote and cache
        if (result == null) {
            result = await super.read(path, offset, size)
            const it = CacheHolder.get(path);

            if (it.lastModificationTime === BigInt(0)) {
                const metadata = await this.metadata(path)
                it.lastModificationTime = (await this.metadata(path)).modificationTime;
                it.fileSize = metadata.size
            }

            it.put(offset, result)
        }

        return result
    }

    // The following require the cache to be emptied
    crypt(path: string, key: string, reverse: boolean): Promise<Modification> {
        return this.ensureCache(path, super.crypt(path, key, reverse), (it) => {
            it.data?.forEachFollowing((chunk) =>
                chunk.data = vigenere(chunk.data, key, reverse)
            );
        });
    }

    write(path: string, offset: bigint, content: Buffer): Promise<Modification> {
        return this.ensureCache(path, super.write(path, offset, content), (it) => {
            // Simple operation, overwrite pre-existing content
            it.put(offset, content)

            if (it.fileSize < offset + BigInt(content.length)) {
                it.fileSize = offset + BigInt(content.length)
            }
        });
    }

    insert(path: string, offset: bigint, content: Buffer): Promise<Modification> {
        // Insert and update cache accordingly
        return this.ensureCache(path, super.insert(path, offset, content), (it) => {
            // Find first item involved
            var chunk = it.data?.locate(offset)

            if (chunk != null) {
                // If we insert in the middle of the chunk, we split the chunk
                if (chunk.offset <= offset) {
                    // Update data
                    const rightChunk = new CachedChunk(
                        offset, 
                        chunk.data.slice(Number(offset))
                    )
                    chunk.data = chunk.data.slice(0, Number(offset))

                    // Update neighbours
                    rightChunk.next = chunk.next
                    chunk.next = rightChunk

                    // Set current chunk to the one on the right to have it's offset moved afterwards
                    chunk = rightChunk
                }

                // Update offset of following chunks
                chunk.forEachFollowing((ch) => ch.offset += BigInt(content.length));
            }

            // The content after the offset was moved, can safely insert the content at the freed location
            it.put(offset, content)

            // Update size
            it.fileSize += BigInt(content.length);
        });
    }

    async delete(path: string) {
        await super.delete(path)

        // No need further check for modification
        CacheHolder.remove(path)
    }

    /**
     * Retrieve up to date metadata, either cached or remote.
     *
     * Ensure the current metadata are up to date
     */
    async metadata(path: string): Promise<Metadata> {
        let entry = CacheHolder.getOrNull(path);
        
        if (entry != null) {
            if (entry.lastModificationTime !== BigInt(0) && !entry.expired) {
                return new Metadata(entry.lastModificationTime, entry.fileSize)
            }
        }

        const remote = await super.metadata(path);
        entry = entry ? entry : CacheHolder.get(path);

        if (remote.modificationTime != entry.lastModificationTime) {
            entry.wipe()
            entry.lastModificationTime = remote.modificationTime
            entry.fileSize = remote.size
        } else {
            entry.refreshed()
        }
        
        return remote
    }

    /**
     * Ensure the cache is still valid for the given file, if so, apply operation on the cache
     */
    private async ensureCache(path: string, modificationTimes: Promise<Modification>, operation: (buffer: CacheEntry) => void): Promise<Modification> {
        const { previousModifiedTime, modifiedTime, finalSize } = await modificationTimes;

        const entry = CacheHolder.get(path);
        
        // Cache was modified between operation and last check
        if (entry.lastModificationTime != previousModifiedTime && entry.lastModificationTime !== BigInt(0)) {
            entry.wipe()
        } else {
            // Otherwise apply the operation on the chunk
            operation(entry)
            entry.refreshed()
        }

        // Update modification according to remote
        entry.lastModificationTime = modifiedTime;
        entry.fileSize = finalSize;

        return modificationTimes
    }

    private async validReadingOf(path: string, offset: bigint, size: number): Promise<Buffer | undefined> {
        const entry = CacheHolder.getOrNull(path);

        if (!entry) {
            return undefined
        } else if (size == -1) { // Remainder of the file
            // If there is a possibility that the data is number the cache
            const cacheLength = entry.readingLengthAfter(offset);

            // Call on metadata ensure cache is up to date
            const remoteSize = (await this.metadata(path)).size;

            // If the cache has the capacity to read this
            if (remoteSize - offset == cacheLength) {
                return entry.read(offset, Number(cacheLength))?.data
            }
        } else { // given size
            if (entry.expired) {
                // Call on metadata will ensure validity
                await this.metadata(path)
            }

            return entry.read(offset, size)?.data
        }
    }
}

