import { InputPattern } from "../../common/patterns"
import COMMANDS from "../command-list"
import inquirer from "inquirer";
import { LogLevel } from "duck-node";

export const commandQuestion = {
	name: "command",
	type: "list",
	message: "Which command do you want to perform",
	choices: Object.keys(COMMANDS).concat("exit")
};

function inputPatternQuestionFactory(inputPattern: InputPattern) {
	return {
		name: "result",
		type: "input",
		message: "Please input value for " + inputPattern.name,
		default: inputPattern.defaultValue,
		validate: (input: string) => input === "" ? inputPattern.optional : inputPattern.pattern.test(input)
	};
}

export async function askFor(inputPattern: InputPattern) {
	const { result } = await inquirer.prompt([ inputPatternQuestionFactory(inputPattern) ]);
	return result;
}

export const logLevelQuestion = {
	name: "logLevel",
	type: "list",
	message: "Which log level do you want to use",
	choices: Object.keys(LogLevel),
	default: "WARNING"
}