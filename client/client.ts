import { parseArgs, LOGGER, LogLevel, duckProperties, DatagramHandler, RemoteObject, RemoteObjectIdentifier } from "duck-node"
import DuckProject from "../target/generated"
import faultySendingFilter from "../common/faulty-socket"
import FilesProperties from "../common/files-share-properties";
import { FileManager } from "../target/generated/remote-objects";
import COMMANDS from "./command-list";
import inquirer = require("inquirer");
import { commandQuestion, askFor, logLevelQuestion } from "./cli/questions";
import CachedFileManagerStub from "./caching/cached-file-manager-stub";
import { HOST_NAME, PORT, InputPattern } from "../common/patterns";
import FileListenerImpl from "./file-listener";
import properties from "duck-node/properties";
import { promises as dns } from 'dns';

export default async function startClient(args: string[]) {
    const parsedArgs = parseArgs(args)
    
    // Change log level
    LOGGER.level = LogLevel[parsedArgs["logLevel"] || (await inquirer.prompt([logLevelQuestion])).logLevel as string];
    LOGGER.colorEnabled = !("noColors" in parsedArgs);

    // Init UDP server
    DatagramHandler.filters.push(faultySendingFilter(parseFloat(parsedArgs["failRate"]) || 0.0));
    await retrieveLocalIp(parsedArgs);
    
    DuckProject.init(
        parseInt(parsedArgs["localPort"]) || 11112
    )

    // Set cache timing
    FilesProperties.cacheTimeout = parseInt(parsedArgs["cachingTimeout"]) || 10000;
    
    // Enable file caching
    RemoteObject.factories[FileManager.name] = (it: RemoteObjectIdentifier) => new CachedFileManagerStub(it);

    // Get address of remote file host
    const fileManager = await retrieveFileManager(parsedArgs)

    FileListenerImpl.get() // register local file listener object after file manager

    let { command } = await inquirer.prompt([ commandQuestion ])

    while (command != null && command != "exit") {
        try {
            await handleCommand(command, fileManager)
                .catch(e => LOGGER.error(e.message));
        } catch (e) {
            LOGGER.error(e.message)
        }

        command = (await inquirer.prompt([ commandQuestion ])).command
    }
}

async function retrieveLocalIp(args: { [key: string]: string }) {
    let ip = args["localIp"];
    
    if (!ip) {
        ip = await askFor(new InputPattern(HOST_NAME.pattern, "local ip address").withDefault("localhost"));
    }

    ip = (await dns.lookup(ip)).address;
    if (ip.includes(".")) {
        properties.localAddress = new Uint8Array(ip.split(".").map(parseFloat));
    } else {
        properties.localAddress = new Uint8Array(ip.split(":").map(parseFloat));
    }
}

/**
 * Ask for details about the file manager
 */
async function retrieveFileManager(args: { [key: string]: string }) {
    let serverIp = args["remoteIp"];
    if (!serverIp) {
        serverIp = await askFor(HOST_NAME.withDefault("localhost"));
    }
    let serverPort = args["remotePort"];
    if (!serverPort) {
        serverPort = await askFor(PORT);
    }

    return RemoteObject.stub<FileManager>(
        await RemoteObjectIdentifier.of(0, serverIp, parseInt(serverPort)),
        FileManager
    )
}

async function handleCommand(commandName: string, fileManager: FileManager) {
    if (commandName in COMMANDS) {
        // Collect args of the command
        const command = COMMANDS[commandName];
        const args = [];
        for (const arg of command.args) {
            args.push(await askFor(arg));
        }

        try {
            const result = await COMMANDS[commandName]?.handler(args, fileManager)

            LOGGER.result(typeof result === "string" ? result : "Done")
        } catch (e) {
            LOGGER.error(e.message)
            LOGGER.info(COMMANDS[commandName].toString().substring(3))
        }
    } else if (commandName != "") {
        LOGGER.info(`command not recognized, available commands (exit for quitting) :\n${ Object.values(COMMANDS).map(it => it.toString()).join("\n") }`)
    }

}
