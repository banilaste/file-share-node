import { FileManager } from "../target/generated/remote-objects"
import { InputPattern } from "../common/patterns";

export class CommandExecutionError extends Error {}

/**
 * Class containing a command line method to be called, check the provided arguments to
 * be constid and provide a descriptive string of the command
 */
export class CommandLineMethod {
    constructor(
        public name: string,
        private description: string,
        public args: InputPattern[],
        public handler: (args: string[], fileManager: FileManager) => Promise<string | void>
    ) {
        let optionalFound = false
        args.forEach(it => {
            if (it.optional) {
                optionalFound = true
            } else if (optionalFound) {
                throw new CommandExecutionError("optional arguments must be the last ones")
            }
        });
    }

    toString(): String {
        const argumentList = this.args
            .map(it => it.optional ? "(" + it.name + ")" : "[" + it.name + "]")
            .join(", ")

        return `\t- ${this.name} ${argumentList} : ${this.description}`
    }


}