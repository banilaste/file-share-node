import { CommandLineMethod, CommandExecutionError } from "./command-line-method";
import { SIZE, OFFSET, FILE_NAME, DURATION, CRYPT_KEY } from "../common/patterns";
import { ByteBuffer, LOGGER } from "duck-node";
import CleanPath from "../common/clean-path";
import inquirer from "inquirer";
import FileListenerObj from "./file-listener";
import FileListenerImpl from "./file-listener";

const COMMANDS: { [key: string]: CommandLineMethod } = {};

/**
 * Map of all commands (transformed into a map in the last line)
 */
const commandList = [
    /**
     * Read method
     */
    new CommandLineMethod(
        "read",
        "read a file from given offset if provided",
        [ FILE_NAME, OFFSET.withDefault("0"), SIZE.withDefault("-1") ],
        (parts, fileManager) => 
            fileManager.read(parts[0], BigInt(parts[1]), parseInt(parts[2]))
                .then(it => it.toString("utf-8"))
    ),
    /**
     * Write method
     */
    new CommandLineMethod(
        "write",
        "write content of a file from a given offset",
        [FILE_NAME, OFFSET.withDefault("0")],
        async (parts, fileManager) => {
            const name = parts[0];
            const offset = BigInt(parts[1]);

            const { content } = await inquirer.prompt([ {
                name: "content",
                type: "input",
                message: "Content to be written",
                validate: (input: string) => input !== ""
            } ]);

            return await fileManager.write(name, offset, Buffer.from(content as string)).then();
        }
    ),
    /**
     * Insert method
     */
    new CommandLineMethod(
        "insert",
        "insert content in a file from a given offset",
        [FILE_NAME, OFFSET.withDefault("0")],
        async (parts, fileManager) => {
            const name = parts[0];
            const offset = BigInt(parts[1]);

            const { content } = await inquirer.prompt([ {
                name: "content",
                type: "input",
                message: "Content to be written",
                validate: (input: string) => input !== ""
            } ]);

            return await fileManager.insert(name, offset, Buffer.from(content as string)).then();
        }
    ),

    /**
     * List file method
     */
    new CommandLineMethod("ls", "list content of a directory", [FILE_NAME.withDefault("/")], (parts, fileManager) => {
        return fileManager.ls(parts[0]?.trim() ? parts[0].trim() : "/").then(it => it.join(", "));
    }),

    /**
     * Create directory method
     */
    new CommandLineMethod("mkdir", "create recursively a directory", [FILE_NAME], (parts, fileManager) => {
        return fileManager.mkdir(parts[0]);
    }),

    /**
     * Watch file method
     */
    new CommandLineMethod("watch", "watch file content for a given ms duration", [FILE_NAME, DURATION], (parts, fileManager) => {
        return fileManager.watch(
            parts[0],
            BigInt(parts[1]),
            FileListenerImpl.get()
        )
    }),
    /**
     * Size of
     */
    new CommandLineMethod("sizeof", "returns the size of a file in bytes", [FILE_NAME], (parts, fileManager) => {
        return fileManager.metadata(parts[0]).then(it => it.size.toString());
    }),
    /**
     * Encrypt
     */
    new CommandLineMethod("crypt", "crypt file using a vigenere like algorithm (really secure !)", [FILE_NAME, CRYPT_KEY], (parts, fileManager) => {
        return fileManager.crypt(parts[0], parts[1], false).then()
    }),
    /**
     * Size of
     */
    new CommandLineMethod("decrypt", "decrypt file using a vigenere like algorithm", [FILE_NAME, CRYPT_KEY], (parts, fileManager) => {
        return fileManager.crypt(parts[0], parts[1], true).then()
    }),
    /**
     * Delete file method
     */
    new CommandLineMethod("delete", "delete file", [FILE_NAME], (parts, fileManager) => {
        return fileManager.delete(parts[0]);
    })
]

commandList.forEach(it => COMMANDS[it.name] = it)

export default COMMANDS;