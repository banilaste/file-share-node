import { FileListener } from "../target/generated/remote-objects";
import { LOGGER, RemoteObjectIdentifier, duckProperties } from "duck-node";

export default class FileListenerImpl extends FileListener {
	private constructor() {
		super(RemoteObjectIdentifier.ofBytes(999, duckProperties.localAddress, duckProperties.localPort));
	}

	changed(path: string, content: Buffer): Promise<void> {
		LOGGER.info(`${path} content changed to : ${content.toString("utf-8")}`);
		return Promise.resolve();
	}

	deleted(path: string): Promise<void> {
		LOGGER.info(`${path} was deleted`);
		return Promise.resolve();
	}

	expired(path: string): Promise<void> {
		LOGGER.info(`${path} is no longer being watched`);
		return Promise.resolve();
	}
	
	static instance: FileListener
	static get() {
		if (!this.instance) {
			this.instance = new FileListenerImpl();
		}

		return this.instance
	}
}