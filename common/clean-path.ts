import FilesProperties from "./files-share-properties";

/**
 * Wrapper for a cleaned path string, this class ensure the string inside is proper to
 * be used in the filesystem (one string per file)
 */
export default class CleanPath {
    cleanPathString: string

    constructor(path: String) {
        const stack: string[] = []

        path.split(/[\\\/]/).forEach(it => {
            switch(it) {
                case "..":
                    if (stack.pop() === undefined) {
                        throw new Error(path + ": invalid path (going above root)");
                    }
                    break;
                case ".": break;
                case "": break;
                default: stack.push(it)
            }
        });

        this.cleanPathString = "/" + stack.reverse().join("/")

    }

    actualPath(): string {
        return FilesProperties.folder + this.cleanPathString;
    }

    toString(): string {
        return this.cleanPathString;
    }

    static of(path: string | CleanPath) {
        if (typeof path === "string") {
            return new CleanPath(path);
        } else {
            return path;
        }
    }
}