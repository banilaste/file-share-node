import { LOGGER } from "duck-node"

/**
 * Factory for a datagram filter emulating network troubles
 *
 * The socket fails to transmit some packet to the destination according to its
 * given rate
 */
export default function faultySendingFilter(failRate: number) {
    if (failRate != 0.0) {
        LOGGER.info("creating faulty datagram socket with fail rate " + failRate)
    }

    return function(message: Buffer, destination: string, port: number) {
        if (failRate == 0.0 || failRate < Math.random()) {
            return true;
        } else {
            LOGGER.debug("sending was randomly failed");
            return false;
        }
    }
}