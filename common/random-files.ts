import { ByteBuffer } from "duck-node"
import CleanPath from "./clean-path"
import fs, { promises } from "fs";

namespace RandomFiles {
    export function open(path: string | CleanPath, mode: string): Promise<promises.FileHandle> {
        if (typeof path === "string") {
            path = new CleanPath(path);
        }

        return promises.open(path.actualPath(), mode)
    }

    /**
     * Read part of file
     */
    export function read(path: String, offset: number, fn: (fh: promises.FileHandle) => number): Promise<Buffer> {
        const cleanPath = new CleanPath(path);
        const realPath = cleanPath.actualPath();

        if (fs.statSync(realPath).isDirectory()) {
            throw Error(cleanPath.toString() + " is a directory, not a file");
        }

        return open(realPath, "r")
            .then(fileHandle => {
                const bytes = Buffer.alloc(fn(fileHandle))
                fileHandle.read(bytes, offset, bytes.length);
                fileHandle.close();
                return bytes;
            });
    }

    /**
     * Write to file asynchronously
     */
    export function write(path: string | CleanPath, offset: number, content: ByteBuffer | Uint8Array, postAction?: (fh: promises.FileHandle) => void): Promise<void> {
        return open(CleanPath.of(path).actualPath(), "rw")
            .then(file => {
                if (offset > file.stat.length) {
                    file.close();
                    throw new Error("offset exceed file size, please write with smaller offset");
                }

                file.write(content instanceof ByteBuffer ? content.buffer : content, offset);

                if (postAction) {
                    postAction(file)
                }

                file.close();
            });
    }
}

export default RandomFiles;